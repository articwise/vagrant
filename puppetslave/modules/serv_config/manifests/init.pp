class serv_config {

file {"/etc/hosts":
        owner => "root",
        group => "root",
        content => template("serv_config/hosts.erb"),
        }

file {"/etc/puppet/puppet.conf":
        owner => "root",
        group => 'root',
        content => template("serv_config/puppet.conf.erb"),
        }

package {"vim":
	ensure => "installed",
	}
package {"telnet":
        ensure => "installed",
        }
              
}
